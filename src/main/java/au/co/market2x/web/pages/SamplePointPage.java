package au.co.market2x.web.pages;

import au.co.market2x.web.cucumber.TestContext;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public abstract class SamplePointPage extends Page {
    By loadingSpinner = new By.ByCssSelector(".LoadingSpinner");
    By refreshbutton = new By.ByCssSelector("button.ant-btn.DefaultPageHeaderButton.ant-btn-sm svg");

    By tableRow = new By.ByCssSelector("tbody > tr");

    By tableCell = new By.ByCssSelector("td");

    By chartCardHeader = new By.ByCssSelector(".ChartCard-header");

    public SamplePointPage(TestContext context) throws Exception {
        super(context);
    }

    public void refreshPage() {
        find.findWebElement(refreshbutton).click();
        find.waitForElementVisibility(refreshbutton);
    }

    public void isLoaded(String expectedValue) {
        find.waitForElementText(pagetitle,expectedValue);
    }

    public void goToDetailsView(int row) throws Exception {
        this.getTableRows().get(row).click();
        find.waitForElementVisibility(chartCardHeader);
    }

    protected List<WebElement> getTableRows() throws Exception {
        return testContext.getTestDriverManager().getDriver().findElements(tableRow);
    }

//    By moreDropdownButton = new By.ByCssSelector("button.ant-btn.DefaultPageHeaderButton.ant-btn-sm.no-border svg");
//
//    public void clickMoreDropdownButton() {
//        find.findWebElement(moreDropdownButton).click();
//    }
}
